//
//  Tests.swift
//  Tests
//
//  Created by kyi moe cho on 2018-05-28.
//  Copyright © 2018 Zabu. All rights reserved.
//

import XCTest
@testable import NonRecursiveMergeSort

class Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testAscending() {
        
        let ar = [7,2,10,2,3,5,1,0,3,2]
        let result = [0,1,2,2,2,3,3,5,7,10]
        
        let s = NonRecursiveMergeSort(ar)
        
        
        assert(s.getAscendingOrder() == result)
        
    }
    
    func testDescending() {
        
        let ar = [7,2,10,2,3,5,1,0,3,2]
        let result = [10,7,5,3,3,2,2,2,1,0]
        
        let s = NonRecursiveMergeSort(ar)
        
        
        assert(s.getDescendingOrder() == result)
        
    }

    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
