//
//  NonRecursiveMergeSort.swift
//  NonRecursiveMergeSort
//
//  Created by kyi moe cho on 2018-05-28.
//  Copyright © 2018 Zabu. All rights reserved.
//
//  This O(nLogn) implementation of bottomup Merge based on Rama Hoetzlein's post
//  on StackOverflow

class NonRecursiveMergeSort<T:Numeric & Comparable> {
    
    private var sortedArray:[T]
    private var unsortedArray:[T]
    private var sortedDesc:[T]
    
    init(_ arrayToBeSorted: [T]) {
        unsortedArray = arrayToBeSorted
        sortedArray = [] // Swift doesn't allow method to be invoked before initializing all properties
        sortedDesc = []
        orderAssend()
        sortedArray = unsortedArray
    }
    
    func getAscendingOrder() -> [T] {
        return sortedArray
    }
    
    // This function takes O(n)
    func getDescendingOrder() -> [T] {
        if sortedDesc == [] {
            sortedDesc = sortedArray.reversed()
        }
        return sortedDesc
    }
    
    private func orderAssend() {
        
        let total = unsortedArray.count
        var windowSize = 1
        var temp:[Int:T] = [:]
        
        // Create window size
        while (windowSize < total) {
            var left = 0
            
            // Make comparisons within defined window size
            while(left + windowSize < total) {
                var m = left
                var index = left
                let right = left + windowSize
                var secondIndex = right
                var rightBound = right + windowSize
                
                if rightBound > total {
                    rightBound = total
                }
                
                // Swap places if necessary. If not copy as it is to the temp dictionary
                while ((index < right) && (secondIndex < rightBound)) {
                    
                    if unsortedArray[index] <= unsortedArray[secondIndex] {
                        temp[m] = unsortedArray[index]
                        index += 1
                    } else {
                        temp[m] = unsortedArray[secondIndex]
                        secondIndex += 1
                    }
                    
                    m += 1
                }
                
                // Copy left over to temp dictionary
                while (index < right) {
                    temp[m] = unsortedArray[index]
                    index += 1
                    m += 1
                }
                
                // Copy left over to temp dictionary
                while (secondIndex < rightBound) {
                    temp[m] = unsortedArray[secondIndex]
                    secondIndex += 1
                    m += 1
                }
                
                m = left
                
                // Overwrite original array elements
                while (m < rightBound) {
                    unsortedArray[m] = temp[m]!
                    m += 1
                }
                
                // Move on to the next group
                left += windowSize*2
                
            }
            
            // Expand window size
            windowSize *= 2
            
        }
        
        
    }
    
    
    
}
